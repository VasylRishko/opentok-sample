var jwt = require('jsonwebtoken');
var express = require('express');
var router = express.Router();

router.get('/session', function (req, res, next) {
  opentok.createSession({ mediaMode: 'routed' }, function(err, session) {
    if (err) return next(err);

    res
      .status(200)
      .json({
        key: opentok.apiKey,
        sessionId: session.sessionId
      });
  });
});

router.get('/archive/:id', function (req, res, next) {
  opentok.getArchive(req.param('id'), function (err, archive) {
    if (err) return next(err);

    res
      .status(200)
      .json({
        archive: archive
      });
  });
});


router.post('/session/start_archive', function (req, res, next) {
  var sessionId = req.param('session');
  
  opentok.startArchive(sessionId, { name: 'LiveSessionTest', type: 'horizontalPresentation' }, function (err, archive) {
    if (err) return next(err);

    res
      .status(200)
      .json({
        archive: archive
      });
  });
});

router.post('/session/stop_archive', function (req, res, next) {
  var sessionId = req.param('session');

  opentok.stopArchive(sessionId, function (err, archive) {
    if (err) return next(err);

    res
      .status(200)
      .json({
        archive: archive
      });
  });
});

router.post('/token', function (req, res, next) {
  var sessionId = req.param('session');

  var apiKey = process.env.OT_API_KEY;
  var apiSecret = process.env.OT_API_SECRET;
  var currentTime = Math.floor(new Date() / 1000);
  var jwtToken = jwt.sign({
    iss: apiKey,
    ist: 'project',
    iat: currentTime,
    exp: currentTime + 3600000
  }, apiSecret);

  try {
    var publisherFocusToken = opentok.generateToken(sessionId, { role: 'publisher', initialLayoutClassList : ['focus'] });
    var publisherToken = opentok.generateToken(sessionId, { role: 'publisher' });
  } catch (e) {
    return res.json(500, { error: e })
  }

  res.status(200).json({
    key: opentok.apiKey,
    sessionId: sessionId,
    publisherFocusToken: publisherFocusToken,
    publisherToken: publisherToken,
    jwtToken: jwtToken
  })
});

module.exports = router;
